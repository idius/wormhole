#! /bin/bash


g++ wormhole.cpp compression.cpp -o wormhole -O2 -pthread -std=c++11 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -Wall -lz && strip wormhole
