#include "compression.hpp"

#include <cstdio>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <cstring>
#include <cassert>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <zlib.h>


#define ZLIB_CHUNK 524288

namespace erb {


const uint kmax_threads = std::thread::hardware_concurrency();
uint nthreads = 0;
std::mutex nthreads_mtx;
std::condition_variable nthreads_cv;


const bool IsValidFile(const char* filename)
{
   struct stat status;

   const int result = stat(filename, &status);

   if(result < 0) { return  false; }

   if (status.st_size < 1) { return false; }

   return true;
}


void Zipper(const std::string infile, const std::string outfile, ZipAction action)
{
   // limit the number of worker threads to the number of hardware threads

   std::unique_lock<std::mutex> lock(nthreads_mtx);

   if (nthreads == kmax_threads)
   {
      while (nthreads == kmax_threads) { nthreads_cv.wait(lock); }

      ++nthreads;
   }
   else { ++nthreads; }

   lock.unlock();

   // perform the compression or decompression

   erb::ZLibFlate file(infile);

   const int zstatus = file.ExecAction(outfile, action);

   if (zstatus < -1) { erb::ZLibFlate::ErrorMessage(zstatus); }

   // remove the input file from the input directory

   unlink(infile.c_str());

   // prepare for the thread to terminate: decrement the total number of threads

   nthreads_mtx.lock();
   --nthreads;
   nthreads_mtx.unlock();

   // notify a waiting thread that this thread is exiting.

   nthreads_cv.notify_one();
}


const int ZLibFlate::Compress(const std::string& outputfile)
{
   return ExecAction(outputfile, kzip);
}


const int ZLibFlate::Decompress(const std::string& outputfile)
{
   return ExecAction(outputfile, kunzip);
}


const int ZLibFlate::ExecAction(const std::string& outputfile, ZipAction action)
{
   if (!IsValidFile(kfile_name_.c_str())) { return Z_ERRNO; }

   FILE* source = fopen(kfile_name_.c_str(), "rb");

   FILE* dest = fopen(outputfile.c_str(), "wb");

   int return_value = Z_ERRNO;

   if (source != nullptr && dest != nullptr)
   {
      if (action == kzip)
      {
         return_value = Compress(source, dest);
      }
      else if (action == kunzip)
      {
         return_value = Decompress(source, dest);
      }

      fclose(source);
      fclose(dest);
   }

   return return_value;
}


const int ZLibFlate::Compress(FILE* source, FILE* dest, int level)
{
   int flush;
   z_stream strm;
   unsigned char input[ZLIB_CHUNK];
   unsigned char output[ZLIB_CHUNK];

   // allocate deflate state
   strm.zalloc = Z_NULL;
   strm.zfree = Z_NULL;
   strm.opaque = Z_NULL;

   int return_code = deflateInit(&strm, level);

   if (return_code != Z_OK) { return return_code; }

   // compress until end of file
   do {
       strm.avail_in = fread(input, 1, ZLIB_CHUNK, source);

       if (ferror(source))
       {
           (void)deflateEnd(&strm);
           return Z_ERRNO;
       }

       flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
       strm.next_in = input;

       do {
           strm.avail_out = ZLIB_CHUNK;
           strm.next_out = output;
           return_code = deflate(&strm, flush);
           assert(return_code != Z_STREAM_ERROR);
           const uint bytes_returned = ZLIB_CHUNK - strm.avail_out;

           if (fwrite(output, 1, bytes_returned, dest) != bytes_returned ||
               ferror(dest))
               {
                   (void)deflateEnd(&strm);

                   return Z_ERRNO;
               }
       } while (strm.avail_out == 0);

       assert(strm.avail_in == 0);     // all input will be used

   } while (flush != Z_FINISH);

   assert(return_code == Z_STREAM_END);

   (void)deflateEnd(&strm); // clean up zlib

   return Z_OK;
}


const int ZLibFlate::Decompress(FILE *source, FILE *dest)
{
   int return_code;
   z_stream strm;
   unsigned char input[ZLIB_CHUNK];
   unsigned char output[ZLIB_CHUNK];

   // allocate inflate state
   strm.zalloc = Z_NULL;
   strm.zfree = Z_NULL;
   strm.opaque = Z_NULL;
   strm.avail_in = 0;
   strm.next_in = Z_NULL;
   return_code = inflateInit(&strm);

   if (return_code != Z_OK) { return return_code; }

   // decompress until deflate stream ends or end of file
   do {
       strm.avail_in = fread(input, 1, ZLIB_CHUNK, source);

       if (ferror(source))
       {
           (void)inflateEnd(&strm);
           return Z_ERRNO;
       }

       if (strm.avail_in == 0) { break; }

       strm.next_in = input;

       // run inflate() on input until output buffer not full
       do {
           strm.avail_out = ZLIB_CHUNK;
           strm.next_out = output;
           return_code = inflate(&strm, Z_NO_FLUSH);
           assert(return_code != Z_STREAM_ERROR);
           switch (return_code)
           {
              case Z_NEED_DICT:
                  return_code = Z_DATA_ERROR;
              case Z_DATA_ERROR: // ?
              case Z_MEM_ERROR:
                  (void)inflateEnd(&strm);
                  return return_code;
           }

           const uint bytes_returned = ZLIB_CHUNK - strm.avail_out;

           if (fwrite(output, 1, bytes_returned, dest) != bytes_returned ||
               ferror(dest))
           {
               (void)inflateEnd(&strm);
               return Z_ERRNO;
           }
       } while (strm.avail_out == 0);

       // done when inflate() says it's done
   } while (return_code != Z_STREAM_END);

   // clean up
   (void)inflateEnd(&strm);

   return return_code == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}


void ZLibFlate::ErrorMessage(int return_code)
{
   std::cerr << "StratOS::ZLib: ";

   switch (return_code)
   {
   case Z_ERRNO:         std::cerr << "error reading file\n"; break;
   case Z_STREAM_ERROR:  std::cerr << "invalid compression level\n"; break;
   case Z_DATA_ERROR:    std::cerr << "invalid deflate data\n"; break;
   case Z_MEM_ERROR:     std::cerr << "out of memory\n"; break;
   case Z_VERSION_ERROR: std::cerr << "zlib version mismatch!\n"; break;
   }
}


} // ns erb

