/// \todo Limit the number of threads that are created---not just the number of
/// threads that actively execute simultaneously. Use a std::queue of
/// command objects {inputfile, outputfile, action}. One thread should act as a
/// manager to run at most kmax_threads threads at once. Adding new commands is then
/// as simple as adding new commands to the queue. When a thread is finished, it can
/// inform the manager so that the manager can run the next command in the queue.

#include "compression.hpp"

#include <iostream>
#include <vector>
#include <string>

#include <sys/inotify.h>
#include <unistd.h>
#include <errno.h>

#define NUM_EVENTS 100

namespace erb { // Einstein-Rosen Bridge

/// Make sure that the filename consists of printable characters.
bool IsPrintable(const std::string& name);

} // ns erb


int main(int argc, char* argv[])
{
   if (argc != 4)
   {
      std::cerr <<
              "Usage: wormhole command entrance exit\n\n"
              "* 'command'' is one of  ([--zip', '--unzip', 'program_name'],\n"
              "   where 'program_name' is the name to a program that processes\n"
              "   an input file and saves the input file to a specified\n"
              "   directory, as in \n\n"
              "       program_name input_file_name output_directory\n\n"
              "* 'entrance' is a directory that will be the input mouth of the\n"
              "   wormhole. Files dropped into this directory will be processed\n"
              "   by the 'command' and will emerge in the 'exit' directory.\n"
              "   Files dropped into this directory will vanish after they have\n"
              "   been processed.\n\n"
              "* 'exit' is the directory where all processed files are sent.\n\n";

      return 1;
   }

   const std::string kcommand(argv[1]);

   const char* entrance_directory = argv[2];

   const char* exit_directory = argv[3];

   // Start inotify and use it to watch the input directory for files that have
   // been moved into the directory or written and closed in the directory.

   int monitor_file_descriptor = inotify_init();

   if (monitor_file_descriptor < 0)
   {
      perror("inotify_init");
      return errno;
   }

   int watch = inotify_add_watch(monitor_file_descriptor,
                                 entrance_directory,
                                 IN_CLOSE_WRITE | IN_MOVED_TO);

   if (watch < 0)
   {
      perror("inotify_add_watch");
      return errno;
   }

   inotify_event event_buffer[NUM_EVENTS];

   int bytes_read;

   // wait for events to happen and handle them as instructed by the user

   while ((bytes_read = read(monitor_file_descriptor,
                             event_buffer,
                             NUM_EVENTS * sizeof(inotify_event))) > 0)
   {
      if (bytes_read < 0)
      {
         perror("read");
         return errno;
      }

      const int actions_read = bytes_read / sizeof(inotify_event);

      std::vector<inotify_event> events(event_buffer, event_buffer + actions_read);

      for (const inotify_event& event : events)
      {
         const std::string filename(event.name);
         const std::string inputfile = std::string(entrance_directory) + "/" + filename;
         const std::string outputfile = std::string(exit_directory) + "/" + filename;

         if (erb::IsPrintable(filename)
             && filename[0] != '.'
             && erb::IsValidFile(inputfile.c_str()))
         {
            if (kcommand != "--zip" && kcommand != "--unzip")
            {
               std::string command = kcommand
                                + " \"" + inputfile + "\""
                                + " \"" + std::string(exit_directory) + "/\"";

               const int exit_status = system(command.c_str());

               if (exit_status != 0)
               {
                  std::cerr << "command \'" << kcommand << "\' exited with status "
                       << exit_status << std::endl;
               }
            }
            else // kcommand == "--zip" or "--unzip"
            {
               const erb::ZipAction action = (kcommand == "--zip")
                                             ? erb::kzip
                                             : erb::kunzip;

               /// \todo this should simply call a function that updates the command
               /// queue. The first call to this function can be used to create the
               /// queue object.
               std::thread(erb::Zipper, inputfile, outputfile, action).detach();
            }
         }
      }
   }

   // There's no need to clean up the inotify watch when the program runs as
   // intended because the inotify file descriptor will be deleted when the
   // program is terminated. The following clean-up code is included for
   // completeness.

   inotify_rm_watch(monitor_file_descriptor, watch);

   close(monitor_file_descriptor);

   return 0;
}

namespace erb {

bool IsPrintable(const std::string& name)
{
   if (name.size() == 0 ) { return false; }

   for (const char c : name)
   {
      if (isprint(c) == 0) { return false; }
   }

   return true;
}

} //ns erb
