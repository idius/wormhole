#ifndef COMPRESS_H
#define COMPRESS_H

#include <string>
#include <thread>

namespace erb { // Einstein-Rosen Bridge

/// ZipAction is used to specify which action should be performed: zip or unzip
/// (i.e., compress or decompress).
enum ZipAction { kzip, kunzip };

/// verify that the file exists and contains data.
const bool IsValidFile(const char* filename);

/// Perform the compression or decompression. Zipper is thread-aware. It will
/// execute in at most kmax_threads simultaneously.
void Zipper(const std::string infile, const std::string outfile, ZipAction action);

class ZLibFlate
{
public:

   /// Construct the object. Specify the file upon which the object will act.
   ZLibFlate(const std::string& inputfile) : kfile_name_(inputfile) {}

   /// Compress the managed file and save it as outputfile. This is equivalent to
   /// ExecAction(outputfile, kzip);
   const int Compress(const std::string& outputfile);

   /// Decompress the managed file and save it as outputfile. This is equivalent to
   /// ExecAction(outputfile, kunzip);
   const int Decompress(const std::string& outputfile);

   /// Perform the specified action (zip or unzip) and save the resulting file as
   /// outputfile.
   const int ExecAction(const std::string& outputfile, ZipAction action);

   /// Compress from file source to file dest until EOF on source
   static const int Compress(FILE *source, FILE *dest, int level = 1);

   /// Decompress from file source to file dest until stream ends or EOF.
   static const int Decompress(FILE *source, FILE *dest);

   /// Translate the error code into a meaningful error message.
   static void ErrorMessage(int return_code);

private:

   // the name of the input file associated with this object.
   const std::string kfile_name_;

};

} //ns erb

#endif // COMPRESS_H
