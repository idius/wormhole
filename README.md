

Wormhole watches a single directory and performs specified actions on any files 
that are moved into or written to the directory. Once the files are processed, 
the results are stored in another directory. The syntax is like this:

```
#!bash

$ wormhole action entrance_directory exit_directory
```

where the action is either a program name or one of the built-in 
commands: --zip or --unzip.

The program that is used as the action must take two input arguments: an input 
file and an output directory. Wormhole expects the action to remove the input 
file from the entrance_directory as its final step.

For more info: http://www.nrstickley.com/wormhole/

**Building:**

You will need the zlib header (the library should already be installed) and either clang++ or g++ to build Wormhole. 

To compile, run the build.sh script. 

